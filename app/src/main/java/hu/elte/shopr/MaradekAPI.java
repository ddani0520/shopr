package hu.elte.shopr;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class MaradekAPI {
    public static final String BASE_URL = "http://10.0.2.2:8080/";

    public static MaradekAPIInterface maradekAPIInterface = null;

    public static MaradekAPIInterface getService()
    {
        if(maradekAPIInterface == null)
        {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            maradekAPIInterface = retrofit.create(MaradekAPIInterface.class);
        }
        return maradekAPIInterface;
    }
    public interface  MaradekAPIInterface {
        @GET("list")
        Call<List<Food>> getFoods(@Query("page") int page);
    }
}
