package hu.elte.shopr;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    FoodItemsDataAdapter foodItemsDataAdapter;
    SwipeController swipeController;
    List<Food> list=new ArrayList<>();
    RecyclerView recyclerView;
    LinearLayoutManager manager;
    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems,page;
    ImageView refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        foodItemsDataAdapter = new FoodItemsDataAdapter(list,this);
        recyclerView = (RecyclerView)findViewById(R.id.fooditemlist);
        setupRecyclerView();
        page=0;
        manager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(foodItemsDataAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = manager.getChildCount();
                totalItems = manager.getItemCount();
                scrollOutItems = manager.findFirstVisibleItemPosition();

                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                {
                    isScrolling = false;
                    page++;
                    getData();
                }
            }
        });

        refresh = (ImageView) findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isScrolling=false;
                page=0;
                list.clear();
                foodItemsDataAdapter.notifyDataSetChanged();
                getData();
            }
        });
        getData();
    }

    private void setupRecyclerView() {

/*
        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                foodItemsDataAdapter.getFoodItems().remove(position);
                foodItemsDataAdapter.notifyItemRemoved(position);
                foodItemsDataAdapter.notifyItemRangeChanged(position, foodItemsDataAdapter.getItemCount());
            }
        },getApplicationContext());

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c);
            }
        });*/
    }

    private void getData()
    {
        Log.println(Log.INFO,"asd","adsasddas");
        final Call<List<Food>> postList = MaradekAPI.getService().getFoods(page);
        postList.enqueue(new Callback<List<Food>>() {
            @Override
            public void onResponse(Call<List<Food>> call, Response<List<Food>> response) {
                Toast.makeText(MainActivity.this, ""+page, Toast.LENGTH_SHORT).show();
                if(response.body().isEmpty()){page--;}//todo megnezni h itt miert valt rosszul paget
                list.addAll(response.body());
                foodItemsDataAdapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, response.body().size()+"Success"+page, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Food>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error Occured"+t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.println(Log.INFO,"aaa",t.getMessage());
            }
        });

    }

}
