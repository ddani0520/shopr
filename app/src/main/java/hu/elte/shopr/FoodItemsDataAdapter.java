package hu.elte.shopr;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FoodItemsDataAdapter extends RecyclerView.Adapter<FoodItemsDataAdapter.FoodItemViewHolder> {
    private List<Food> foodItems;
    private Context context;

    public class FoodItemViewHolder extends RecyclerView.ViewHolder {
        private TextView itemName, itemPrice, itemLocation, itemFromTo, itemPhoneNum,percent;
        private ImageView itemImage;

        public FoodItemViewHolder(View view) {
            super(view);
            itemName = (TextView) view.findViewById(R.id.itemName);
            itemImage = (ImageView) view.findViewById(R.id.itemImage);
            itemPrice = (TextView) view.findViewById(R.id.itemPrice);
            itemLocation = (TextView) view.findViewById(R.id.itemLocation);
            itemFromTo = (TextView) view.findViewById(R.id.itemDiscDate);
            itemPhoneNum = (TextView) view.findViewById(R.id.itemPhoneNum);
            percent = (TextView) view.findViewById(R.id.percent);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    phoneCall("tel:"+itemPhoneNum.getText().toString().trim());
                }
            });

        }

        private void phoneCall(String pn){
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse(pn));
            context.startActivity(intent);
        }
    }

    public FoodItemsDataAdapter(List<Food> foodItems, Context context) {
        this.foodItems = foodItems;
        this.context = context;
    }

    @Override
    public FoodItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_item, parent, false);

        return new FoodItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FoodItemViewHolder holder, int position) {
        Food foodItem = foodItems.get(position);

        Calendar fromc= Calendar.getInstance();
        fromc.setTimeInMillis(foodItem.getDiscountedFrom());

        Calendar toc= Calendar.getInstance();
        toc.setTimeInMillis(foodItem.getDiscountedTo());

        holder.itemName.setText(foodItem.getTitle()+"");
        Picasso.get()
                .load("https://coresites-cdn.factorymedia.com/cooler_new/wp-content/uploads/2015/07/best-looking-street-food-trucks-7.jpg")
                .into(holder.itemImage);
        holder.itemPrice.setText(foodItem.getDiscount()+" Ft");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        holder.itemFromTo.setText(simpleDateFormat.format(fromc.getTime())+" - "+simpleDateFormat.format(toc.getTime()));
        holder.itemLocation.setText(foodItem.getAddress());
        holder.itemPhoneNum.setText(foodItem.getPhoneNumber());
        double disc = ((double)(foodItem.getDiscount())/(double)(foodItem.getPrice()))*100;
        holder.percent.setText("-"+Math.floor((100-disc)*100)/100+"%");
    }

    @Override
    public int getItemCount() {
        return foodItems.size();
    }

    public List<Food> getFoodItems() {
        return foodItems;
    }
}
