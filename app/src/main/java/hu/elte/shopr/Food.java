package hu.elte.shopr;

import android.graphics.Bitmap;

import java.util.Date;

public class Food {
    private String title;
    private String imageUrl;
    private String phoneNumber;
    private int price;
    private long discountedFrom;
    private long discountedTo;
    private String address;
    private int discount;

    public Food(String title, String imageUrl, String phoneNumber, int price, long from, long to, String address,int discount) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.phoneNumber = phoneNumber;
        this.price = price;
        this.discountedFrom = from;
        this.discountedTo = to;
        this.address = address;
        this.discount = discount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getDiscountedFrom() {
        return discountedFrom;
    }

    public void setDiscountedFrom(long discountedFrom) {
        this.discountedFrom = discountedFrom;
    }

    public long getDiscountedTo() {
        return discountedTo;
    }

    public void setDiscountedTo(long discountedTo) {
        this.discountedTo = discountedTo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDiscount() { return discount; }

    public void setDiscount(int discount) { this.discount = discount; }
}
